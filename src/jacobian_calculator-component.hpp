#ifndef OROCOS_JACOBIANCALCULATOR_COMPONENT_HPP
#define OROCOS_JACOBIANCALCULATOR_COMPONENT_HPP

#include <rtt/RTT.hpp>

#include <motion_control_msgs/JointEfforts.h>
#include <motion_control_msgs/JointPositions.h>
#include <sensor_msgs/JointState.h>


#include <kdl/expressiontree.hpp>
#include <expressiongraph/urdfexpressions.hpp>
#include <kdl/frames.hpp>

#include <vector>
#include <Eigen/Core>

const int CART_VEC_DIM = 6;

enum PortMessageType {
    VECTOR              = 0, ///< standard vector
    ROS                 = 1, ///< ros sensor_msgs/JointState
};

class Jacobian_calculator : public RTT::TaskContext{
public:
    Jacobian_calculator(std::string const& name);
    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();

    bool checkJointNames();
    void calibrate();
    int getNumberOfJoints();
	void setWrenchOffset();
	void filterDeadZone();
	
private:

    // ports
    RTT::InputPort< sensor_msgs::JointState >     joint_state_js_inport_; 
    RTT::InputPort< std::vector<double> >         joint_pos_vec_inport_; 
    RTT::InputPort< std::vector<double> >         joint_vel_vec_inport_; 
    RTT::InputPort< std::vector<double> >         joint_eff_vec_inport_; 
    RTT::OutputPort< double >     tool_fx_outport_; 
    RTT::OutputPort< double >     tool_fy_outport_; 
    RTT::OutputPort< double >     tool_fz_outport_; 
    RTT::OutputPort< double >     tool_tx_outport_; 
    RTT::OutputPort< double >     tool_ty_outport_; 
    RTT::OutputPort< double >     tool_tz_outport_; 
    RTT::OutputPort< double >     joint_torque_1_outport_; 
    RTT::OutputPort< double >     joint_torque_2_outport_; 
    RTT::OutputPort< double >     joint_torque_3_outport_; 
    RTT::OutputPort< double >     joint_torque_4_outport_; 
    RTT::OutputPort< double >     joint_torque_5_outport_; 
    RTT::OutputPort< double >     joint_torque_6_outport_; 
    RTT::OutputPort< double >     joint_torque_7_outport_; 
    RTT::OutputPort< KDL::Wrench >                cart_wrench_base_kdl_outport_, cart_wrench_ee_kdl_outport_; 
    RTT::OutputPort< std::vector<double> >      cart_wrench_base_vec_outport_, cart_wrench_ee_vec_outport_; 

    // debug ports 
    RTT::OutputPort< KDL::Frame >      cart_pos_kdl_outport_; 
    RTT::OutputPort< KDL::Twist >      cart_vel_kdl_outport_; 

    // properties
    std::string p_urdf_file_;
    std::string p_urdf_base_;
    std::string p_urdf_ee_;
    int         p_solution_method_;
    int         p_message_type_;
    double p_lpf_alpha_;


    // variables used to read from / write to ports
    sensor_msgs::JointState   m_joint_state_js_; 
    std::vector<double>       m_torque_offset_vec_;
    KDL::Wrench               m_cart_wrench_base_kdl_, m_cart_wrench_ee_kdl_, m_cart_wrench_ee_kdl_filt_, wrench_offset;
    std::vector<double>       m_cart_wrench_base_vec_, m_cart_wrench_ee_vec_;
    std::vector<double>     m_joint_pos_vec_;
    std::vector<double>     m_joint_vel_vec_;
    std::vector<double>     m_joint_eff_vec_;

    KDL::Twist m_cart_twist_kdl_;
    
    Eigen::Matrix<double,       7     , 1>  joint_eff_eigen_, joint_vel_eigen_; 
    Eigen::Matrix<double, CART_VEC_DIM, 1>  cart_wrench_eigen_, cart_wrench_eigen_filt_, cart_vel_eigen_;
    
    /**
    * urdf(double K_limits, velocity_scale)
    * you can turn of position limits by setting K_limits==0
    * you can turn of velocity limits by setting velocity_scale==0
    */
    KDL::UrdfExpr3 urdf_;
    KDL::Context::Ptr ctx_;
    KDL::Expression<KDL::Frame>::Ptr kin_chain_;

    KDL::Frame T_ee_base_;
    
    KDL::Twist jacobian_col_;
    Eigen::Matrix<double,CART_VEC_DIM,7>            jacobian_;
    Eigen::Matrix<double,7,CART_VEC_DIM>            jacobian_t_;
    Eigen::Matrix<double,CART_VEC_DIM,CART_VEC_DIM> jacobian_sq_;

    std::vector<std::string> joint_names_, link_names_;
    std::vector<int> joint_idx_;
    std::vector<double> joint_values_;
    int NUMBER_OF_JOINTS;
    bool joint_names_correct_;

    PortMessageType       message_type_;
    RTT::FlowStatus port_status_;

    int counter_;
    int inverted_wrench; 
    RTT::os::TimeService::ticks start_time_;
	
    double deadzone_fx; 
    double deadzone_fy;
    double deadzone_fz;
    double deadzone_tx;
    double deadzone_ty;
    double deadzone_tz;	
};

#endif


