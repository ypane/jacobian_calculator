
#include <kdl/expressiontree.hpp>
// #include <expressiongraph/context.hpp>
// #include <expressiongraph/context_aux.hpp>
#include <expressiongraph/urdfexpressions.hpp>

/**
 * An example of a <double> expression in multiple variables.
 */
int main(int argc, char* argv[]) {

    KDL::Context::Ptr ctx = KDL::create_context();
    KDL::UrdfExpr3 urdf(10.0,1.0);
    std::vector<std::string> joint_names(7), link_names;
    std::vector<int> joint_idx(joint_names.size());
    std::vector<double> joint_values(joint_names.size());
    std::string urdf_file = "/home/keivan/finrop_ws/src/deploy_contour_tracker/urdf/iiwa14.urdf";


    // set-up necessary for robot control problems:
    // time and type time are defined by default.
    ctx->addType("robot");
    ctx->addType("feature");
    std::cout << "parsing urdf file located at:\n" << urdf_file << std::endl;
    urdf.readFromFile(urdf_file);
    urdf.addTransform("ee","iiwa_link_ee_pneumatic","iiwa_link_0");

    KDL::ExpressionMap m = urdf.getExpressions(ctx);
    KDL::Expression<KDL::Frame>::Ptr kinchain;
    for (KDL::ExpressionMap::iterator it = m.begin(); it!=m.end(); ++it) {
        std::cout << it->first << "\t";
        std::cout << std::endl;
        
        // it->second->print(std::cout);
        kinchain = it->second;
    }

    urdf.getAllJointNames(joint_names);
    link_names = urdf.getLinkNames();
    // ctx->printInputChannels(std::cout);

    std::cout << "\njoint names: " << std::endl;
    for (int i=0; i<joint_names.size(); i++) 
        std::cout << joint_names[i] << ", " ;
    
    std::cout << "\n--------------------------------- " << std::endl;
    std::cout << "\nlink names: " << std::endl;
    for (int i=0; i<link_names.size(); i++) 
        std::cout << link_names[i] << ", " ;
    std::cout << std::endl;
    

    std::cout << "joint index in context: " << std::endl;
    for (int i=0; i<joint_names.size(); i++)  {
        joint_idx[i] = ctx->getScalarNdx(joint_names[i]);
        std::cout << " " << joint_idx[i] ;
    }
    std::cout << " \n" << std::endl;


    for (int i=0; i<joint_names.size(); i++)  {
        joint_values[i] = i*0.0;
    }
    joint_values[1] = 5.26*M_PI/180;
    joint_values[5] = 11.62*M_PI/180;
    kinchain->setInputValues(joint_idx,joint_values);

    std::cout << "--- Pose of the end effector of the kinematic chain ---" << std::endl;
    // remember: always call setInputValues() before value() 
    std::cout << "Position of end effector: \n" << kinchain->value() << "\n" << std::endl;
    // remember: always call value() before derivative()
    for (int i=0;i< joint_idx.size(); ++i) {
        std::cout << "jac towards joint " << i << kinchain->derivative(i) << std::endl;
    }

    return 0;
}
