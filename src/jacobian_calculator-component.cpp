#include "jacobian_calculator-component.hpp"
#include <rtt/Component.hpp>
#include <rtt/Logger.hpp>

#include <iostream>


Jacobian_calculator::Jacobian_calculator(std::string const& name) : TaskContext(name)
, p_urdf_file_("")
, p_urdf_base_("iiwa_link_0")
, p_urdf_ee_("iiwa_link_ee_pneumatic")
, p_solution_method_(1)
, p_lpf_alpha_(0.9) // low pass filter coefficiet
, m_torque_offset_vec_(7,0)
, message_type_(VECTOR)
, m_cart_wrench_base_vec_(CART_VEC_DIM,0)
, m_cart_wrench_ee_vec_(CART_VEC_DIM,0)
, inverted_wrench(0)
{

    //Adding properties
    this->addProperty("urdf_file",     p_urdf_file_).doc("Path to urdf file containing the robot information");

    this->addProperty("urdf_base",     p_urdf_base_).doc("Name of the first link of the robot in urdf");
    this->addProperty("urdf_ee",   p_urdf_ee_).doc("Name of the desired end effector in urdf");
    this->addProperty("solution_method",   p_solution_method_)
        .doc("system of linear equations solution method, colPivHouseholderQr(0), pseudoinverse (1)");

    this->addProperty("message_type",     p_message_type_).doc("Inport message type, vector (0), sensor_msgs::JointState(1)");

    this->addProperty("filter_coeff", p_lpf_alpha_).doc("the low pass filter coefficient alpha = dt/(dt+tau)");

    this->addProperty("deadzone_fx", deadzone_fx).doc("the deadzone value for the force in x direction");
    this->addProperty("deadzone_fy", deadzone_fy).doc("the deadzone value for the force in y direction");
    this->addProperty("deadzone_fz", deadzone_fz).doc("the deadzone value for the force in z direction");
    this->addProperty("deadzone_tx", deadzone_tx).doc("the deadzone value for the torque in x direction");
    this->addProperty("deadzone_ty", deadzone_ty).doc("the deadzone value for the torque in y direction");
    this->addProperty("deadzone_tz", deadzone_tz).doc("the deadzone value for the torque in z direction");
    this->addProperty("inverted_wrench", inverted_wrench).doc("whether to multiply the wrench values with -1 or not");

    // Ports
    this->addPort("joint_state_js_inport",    joint_state_js_inport_)
        .doc("current joint state, sensor_msgs::JointState format. Both effort and position field have to contain information.");

    this->addPort("joint_pos_vec_inport",    joint_pos_vec_inport_).doc("current joint position, standard vector format.");
    this->addPort("joint_vel_vec_inport",    joint_vel_vec_inport_).doc("current joint velocity, standard vector format.");
    this->addPort("joint_eff_vec_inport",    joint_eff_vec_inport_).doc("current joint effort, standard vector format.");
    
    this->addPort("cart_wrench_base_kdl_outport",    cart_wrench_base_kdl_outport_)
    .doc("Cartesian wrench [N/Nm] in base frame, KDL::Wrench format");
    this->addPort("cart_wrench_base_vec_outport",    cart_wrench_base_vec_outport_)
    .doc("Cartesian wrench [N/Nm] in base frame, \n std::vector format [fx fy fz mx my mz]");

    this->addPort("cart_wrench_ee_kdl_outport",    cart_wrench_ee_kdl_outport_)
    .doc("Cartesian wrench [N/Nm] in desired end effector frame, KDL::Wrench format");
    this->addPort("cart_wrench_ee_vec_outport",    cart_wrench_ee_vec_outport_)
    .doc("Cartesian wrench [N/Nm] in desired end effector frame, \n std::vector format  [fx fy fz mx my mz]");

    this->addPort("cart_pos_kdl_outport",    cart_pos_kdl_outport_).doc("Cartesian pose [m] of end effector wrt base frame, KDL::Frame format.");
    this->addPort("cart_vel_kdl_outport",    cart_vel_kdl_outport_).doc("Cartesian velocity of end effector wrt base frame, KDL::Twist format.");

    this->addPort("tool_fx",    tool_fx_outport_).doc("tool wrench element - fx");
    this->addPort("tool_fy",    tool_fy_outport_).doc("tool wrench element - fy");
    this->addPort("tool_fz",    tool_fz_outport_).doc("tool wrench element - fz");
    this->addPort("tool_tx",    tool_tx_outport_).doc("tool wrench element - tx");
    this->addPort("tool_ty",    tool_ty_outport_).doc("tool wrench element - ty");
    this->addPort("tool_tz",    tool_tz_outport_).doc("tool wrench element - tz");

    this->addPort("joint_torque_1",    joint_torque_1_outport_).doc("joint 1 torque measurement [Nm]");
    this->addPort("joint_torque_2",    joint_torque_2_outport_).doc("joint 2 torque measurement [Nm]");
    this->addPort("joint_torque_3",    joint_torque_3_outport_).doc("joint 3 torque measurement [Nm]");
    this->addPort("joint_torque_4",    joint_torque_4_outport_).doc("joint 4 torque measurement [Nm]");
    this->addPort("joint_torque_5",    joint_torque_5_outport_).doc("joint 5 torque measurement [Nm]");
    this->addPort("joint_torque_6",    joint_torque_6_outport_).doc("joint 6 torque measurement [Nm]");
    this->addPort("joint_torque_7",    joint_torque_7_outport_).doc("joint 7 torque measurement [Nm]");

    // operations
    this->addOperation("getNumberOfJoints", &Jacobian_calculator::getNumberOfJoints, this, RTT::OwnThread)
        .doc("Returns NUMBER_OF_JOINTS. ");

    this->addOperation("calibrate", &Jacobian_calculator::calibrate, this, RTT::OwnThread)
            .doc("Change the torque offset by reading joint torque values. ");
	
	this->addOperation("setWrenchOffset", &Jacobian_calculator::setWrenchOffset, this, RTT::OwnThread)
			.doc("set the wrench offset for zeroing purpose");

    // create context, needed for expressiongraphs
    ctx_ = KDL::create_context();

    KDL::SetToZero(m_cart_wrench_base_kdl_);
    KDL::SetToZero(m_cart_wrench_ee_kdl_);
    KDL::SetToZero(m_cart_wrench_ee_kdl_filt_);
	KDL::SetToZero(wrench_offset);

    // set dummy sample at the port for dummy reasons
    cart_wrench_base_kdl_outport_.setDataSample(m_cart_wrench_base_kdl_);
    cart_wrench_base_vec_outport_.setDataSample(m_cart_wrench_base_vec_);

    cart_wrench_ee_kdl_outport_.setDataSample(m_cart_wrench_ee_kdl_);
    cart_wrench_ee_kdl_outport_.setDataSample(m_cart_wrench_ee_kdl_filt_);
    cart_wrench_ee_vec_outport_.setDataSample(m_cart_wrench_ee_vec_);

    joint_names_correct_ = false;

}

bool Jacobian_calculator::configureHook(){
    switch (p_message_type_) 
    {
        case 0:
            message_type_ = VECTOR;
            break;
        case 1:
            message_type_ = ROS;
            break;
    }
    // urdf_.poslimits = false;
    // urdf_.vellimits = false;

    // set-up necessary for robot control problems:
    // time and type time are defined by default.
    ctx_->addType("robot");
    ctx_->addType("feature");
    try {
        urdf_.readFromFile(p_urdf_file_);
    } catch (...){
        RTT::Logger::In in(this->getName());
        RTT::log(RTT::Error) << " Parsing urdf file failed. Is the path correct? " << RTT::endlog();

        return false;
    }
    urdf_.addTransform("T_ee_base",p_urdf_ee_,p_urdf_base_);
    // urdf_.addTransform("T_ee_base",p_urdf_ee_,p_urdf_ee_);
    urdf_.getAllJointNames(joint_names_);

    KDL::ExpressionMap m = urdf_.getExpressions(ctx_);
    for (KDL::ExpressionMap::iterator it = m.begin(); it!=m.end(); ++it) {
//         std::cout << it->first << "\t";
//         std::cout << std::endl;
        // it->second->print(std::cout);
        kin_chain_ = it->second;
    }


    // get the joint names from urdf file 
    urdf_.getAllJointNames(joint_names_);
    NUMBER_OF_JOINTS = joint_names_.size();
    joint_idx_.resize(NUMBER_OF_JOINTS);
//     std::cout << "NUMBER_OF_JOINTS: " << NUMBER_OF_JOINTS << std::endl;
    link_names_ = urdf_.getLinkNames();

//     // print information about urdf file
//     std::cout << "\n--------------------------------- " << std::endl;
//     std::cout << "link names: " << std::endl;
//     for (int i=0; i< link_names_.size(); i++) 
//         std::cout << link_names_[i] << "  " ;
// 
// 
//     std::cout << "\n\njoint names: " << std::endl;
//     for (int i=0; i< joint_names_.size(); i++) 
//         std::cout << joint_names_[i] << "  " ;
    
//     std::cout << "\n\njoint index in context: " << std::endl;
    for (int i=0; i< joint_names_.size(); i++)
    {
        joint_idx_[i] = ctx_->getScalarNdx(joint_names_[i]);
//         std::cout << " " << joint_idx_[i] ;
    }
//     std::cout << "\n--------------------------------- " << std::endl;



    // initialize variables needed for the ports
    m_joint_state_js_.name.resize(NUMBER_OF_JOINTS);

    m_torque_offset_vec_.resize(NUMBER_OF_JOINTS);
    m_torque_offset_vec_.assign(NUMBER_OF_JOINTS,0);

    // p_torque_offset_vec_.resize(NUMBER_OF_JOINTS);
    // p_torque_offset_vec_.assign(NUMBER_OF_JOINTS,0);

    joint_values_.resize(NUMBER_OF_JOINTS);
    jacobian_.resize(  CART_VEC_DIM,     NUMBER_OF_JOINTS);
    jacobian_t_.resize(NUMBER_OF_JOINTS, CART_VEC_DIM);

    m_joint_pos_vec_.resize(NUMBER_OF_JOINTS);
    m_joint_vel_vec_.resize(NUMBER_OF_JOINTS);
    m_joint_eff_vec_.resize(NUMBER_OF_JOINTS);

    cart_wrench_eigen_.setZero(CART_VEC_DIM, 1);
    cart_wrench_eigen_filt_.setZero(CART_VEC_DIM, 1);

    std::cout << "jacobian_calculator configured !" <<std::endl;
    return true;
}

bool Jacobian_calculator::startHook(){
    counter_ = 0;
    std::cout << "jacobian_calculator started !" <<std::endl;
    start_time_ = RTT::os::TimeService::Instance()->getTicks();


    return true;
}

void Jacobian_calculator::updateHook(){

    if (message_type_ == VECTOR)
    {
        port_status_ = joint_pos_vec_inport_.read(m_joint_pos_vec_);

        if ( port_status_!= RTT::NoData)
        {
            port_status_ = joint_vel_vec_inport_.read(m_joint_vel_vec_);
            port_status_ = joint_eff_vec_inport_.read(m_joint_eff_vec_);

            kin_chain_->setInputValues(joint_idx_,m_joint_pos_vec_);

            // check names
            if ( !joint_names_correct_)
                joint_names_correct_ = checkJointNames();

            kin_chain_->setInputValues(joint_idx_,m_joint_pos_vec_);
        
            // std::cout << "--- Pose of the end effector of the kinematic chain ---" << std::endl;
            // remember: always call setInputValues() before value() 
            T_ee_base_ = kin_chain_->value();

            // std::cout << "Position of end effector : " << T_ee_base_ << std::endl;
            // remember: always call value() before derivative()
            for (int jnt_col_idx=0; jnt_col_idx< NUMBER_OF_JOINTS; jnt_col_idx++) 
            {
                // derivative gives a KDL::twist back towards joint: jnt_col_idx
                jacobian_col_ = kin_chain_->derivative(joint_idx_[jnt_col_idx]);
                // std::cout << "jac towards joint " << jnt_col_idx << jacobian_col_ << std::endl;

                for (int row_idx=0; row_idx< CART_VEC_DIM/2; row_idx++) 
                {
                    // joint_idx_[jnt_col_idx] is needed to take write on the right column
                    // joint_idx_ is not necessarily the same as jnt_col_idx
                    jacobian_(row_idx   , jnt_col_idx) = jacobian_col_.vel(row_idx);
                    jacobian_(row_idx+3 , jnt_col_idx) = jacobian_col_.rot(row_idx);
                }

                // subtract the joint torque offset from the measured joint 
                m_joint_eff_vec_[jnt_col_idx] -= m_torque_offset_vec_[jnt_col_idx];
            }

            jacobian_t_ = jacobian_.transpose();

            // more information about solving the linear system of equations
            // https://eigen.tuxfamily.org/dox/group__TutorialLinearAlgebra.html
            
            // double elapsed =  RTT::os::TimeService::Instance()->secondsSince( start_time_);
            
            if (0)
            {

                std::cout << "-------------------------------------" << std::endl;
                std::cout << "jacobian: " << std::endl;

                for (int jnt_col_idx=0; jnt_col_idx < NUMBER_OF_JOINTS; jnt_col_idx++) 
                {
                    for (int row_idx=0; row_idx < CART_VEC_DIM; row_idx++) 
                    {
                        std::cout << jacobian_(row_idx  ,jnt_col_idx) << " ";
                    }
                    std::cout << "\n";
                }
                std::cout << std::endl;

                counter_ = 0;
                start_time_ = RTT::os::TimeService::Instance()->getTicks();
            }

            // J' * Wrench = Torque
            joint_eff_eigen_ = Eigen::VectorXd::Map(&m_joint_eff_vec_[0], NUMBER_OF_JOINTS); 
            if (p_solution_method_ == 0)
            {
                cart_wrench_eigen_ = jacobian_t_.colPivHouseholderQr().solve(joint_eff_eigen_);
            }
            else if (p_solution_method_ == 1)
            {
                jacobian_sq_ = jacobian_ * jacobian_t_;
                cart_wrench_eigen_ = jacobian_sq_.inverse() * jacobian_ * joint_eff_eigen_;
            }



            joint_vel_eigen_ = Eigen::VectorXd::Map(&m_joint_vel_vec_[0], NUMBER_OF_JOINTS); 
            cart_vel_eigen_  = jacobian_ * joint_vel_eigen_;
            
        } // port_status_

    }
    else
    {
        port_status_ = joint_state_js_inport_.read(m_joint_state_js_);
        
        if ( port_status_!= RTT::NoData)
        {
			// get joint velocities
			joint_vel_vec_inport_.read(m_joint_vel_vec_);
			// map the joint velocities to eigen matrix type		
			joint_vel_eigen_ = Eigen::VectorXd::Map(&m_joint_vel_vec_[0], NUMBER_OF_JOINTS); 

						
            // check names
            if ( !joint_names_correct_)
                joint_names_correct_ = checkJointNames();
	   	    
	    
            kin_chain_->setInputValues(joint_idx_,m_joint_state_js_.position);
        
            // std::cout << "--- Pose of the end effector of the kinematic chain ---" << std::endl;
            // remember: always call setInputValues() before value() 
            T_ee_base_ = kin_chain_->value();

//            std::cout << "Position of end effector : " << T_ee_base_ << std::endl;
            //std::cout << "Position of end effector : " << joint_idx_[0] << " "  << joint_idx_[1] << " "  << joint_idx_[2] << " " << std::endl;
            // remember: always call value() before derivative()
            for (int jnt_col_idx=0; jnt_col_idx< joint_idx_.size(); jnt_col_idx++) {
                // derivative gives a KDL::twist back towards joint: jnt_col_idx
                jacobian_col_ = kin_chain_->derivative(joint_idx_[jnt_col_idx]);
                // std::cout << "jac towards joint " << jnt_col_idx << jacobian_col_ << std::endl;

                for (int row_idx=0; row_idx< CART_VEC_DIM/2; row_idx++) 
                {
                    jacobian_(row_idx  ,jnt_col_idx) = jacobian_col_.vel(row_idx);
                    jacobian_(row_idx+3,jnt_col_idx) = jacobian_col_.rot(row_idx);
                }

                // subtract the joint torque offset from the measured joint 
                m_joint_state_js_.effort[jnt_col_idx] -= m_torque_offset_vec_[jnt_col_idx];
            }
            jacobian_t_ = jacobian_.transpose();

            // more information about solving the linear system of equations
            // https://eigen.tuxfamily.org/dox/group__TutorialLinearAlgebra.html
            

            // J' * Wrench = Torque
            joint_eff_eigen_ = Eigen::VectorXd::Map(&m_joint_state_js_.effort[0], NUMBER_OF_JOINTS); 
            if (p_solution_method_ == 0)
            {
                cart_wrench_eigen_ = jacobian_t_.colPivHouseholderQr().solve(joint_eff_eigen_);
            }
            else if (p_solution_method_ == 1)
            {
                jacobian_sq_ = jacobian_ * jacobian_t_;
                cart_wrench_eigen_ = jacobian_sq_.inverse() * jacobian_ * joint_eff_eigen_;
            }
            
            // calculate the cartesian velocities
			cart_vel_eigen_  = jacobian_ * joint_vel_eigen_;

        } // port_status_

    } // message_type_

    // -----------------
    // feed the calculated wrench to a low pass filter
    if (isnan(cart_wrench_eigen_filt_[0]))
	{
		cart_wrench_eigen_filt_ = cart_wrench_eigen_;
	}
	else
	{
		cart_wrench_eigen_filt_ =  p_lpf_alpha_  * cart_wrench_eigen_ + 
							(1 - p_lpf_alpha_) * cart_wrench_eigen_filt_ ;
	}

    for (int i=0; i< CART_VEC_DIM/2; i++)
    {
        m_cart_wrench_base_kdl_.force[i]  = cart_wrench_eigen_filt_(i);
        m_cart_wrench_base_kdl_.torque[i] = cart_wrench_eigen_filt_(i+3);

        m_cart_twist_kdl_.vel[i] = cart_vel_eigen_(i);
        m_cart_twist_kdl_.rot[i] = cart_vel_eigen_(i+3);
    }

    // ---------------------------------------
    // Transform the wrench from base frame with reference point at the origin of the base frame
    // to the ee frame with origin in ee frame should be done in two steps:
    // 1- translation , 2- Rotation

    // W_ee =  P M W_base
    // P = [Rot | 0
    //      0   | Rot]
    // 
    // M = [I   | 0
    //      [px]| I]
    // 
    // [px] = [0    -p_z   p_y
    //         p_z   0    -p_x
    //        -p_y   p_x   0]

    // where P is a projection matrix and M the translation matrix.

    // In this file, Jacobian is ee_base_J (with reference point at the origin of ee frame), and hence Wrench is also base_ee_W 
    // meaning that the wrench is expressed at the origin of ee.
    // Therefore no translation is required. Only rotation should be applied

    // TRANSLATION
    // m_cart_wrench_ee_kdl_.force[0] = m_cart_wrench_base_kdl_.force[0];
    // m_cart_wrench_ee_kdl_.force[1] = m_cart_wrench_base_kdl_.force[1];
    // m_cart_wrench_ee_kdl_.force[2] = m_cart_wrench_base_kdl_.force[2];
    // --> m_x = -p_z*f_y + p_y*f_z + m_x
    // m_cart_wrench_ee_kdl_.torque[0] = -T_ee_base_.p[2] * m_cart_wrench_base_kdl_.force[1]  + T_ee_base_.p[1] * m_cart_wrench_base_kdl_.force[2] + m_cart_wrench_base_kdl_.torque[0];
    // m_cart_wrench_ee_kdl_.torque[1] =  T_ee_base_.p[2] * m_cart_wrench_base_kdl_.force[0]  - T_ee_base_.p[0] * m_cart_wrench_base_kdl_.force[2] + m_cart_wrench_base_kdl_.torque[1];
    // m_cart_wrench_ee_kdl_.torque[2] = -T_ee_base_.p[1] * m_cart_wrench_base_kdl_.force[0]  + T_ee_base_.p[0] * m_cart_wrench_base_kdl_.force[1] + m_cart_wrench_base_kdl_.torque[2];

    //  ROTATION 
    // ee_W = M_base_ee * base_W
    if (inverted_wrench == 0)
    {
       m_cart_wrench_ee_kdl_.force  = T_ee_base_.M.Inverse() * m_cart_wrench_base_kdl_.force;
       m_cart_wrench_ee_kdl_.torque = T_ee_base_.M.Inverse() * m_cart_wrench_base_kdl_.torque;
    }
    else
    {
       m_cart_wrench_ee_kdl_.force  = -(T_ee_base_.M.Inverse() * m_cart_wrench_base_kdl_.force);
       m_cart_wrench_ee_kdl_.torque = -(T_ee_base_.M.Inverse() * m_cart_wrench_base_kdl_.torque);
    }
    // ---------------------------------------

    // change the format to vector
    for (unsigned int i=0; i< CART_VEC_DIM/2; i++)
    {
        m_cart_wrench_base_vec_[i] = m_cart_wrench_base_kdl_.force[i];
        m_cart_wrench_base_vec_[i+3] = m_cart_wrench_base_kdl_.torque[i];

        m_cart_wrench_ee_vec_[i] = m_cart_wrench_ee_kdl_.force[i];
        m_cart_wrench_ee_vec_[i+3] = m_cart_wrench_ee_kdl_.torque[i];
    }

    cart_wrench_base_kdl_outport_.write(m_cart_wrench_base_kdl_);
    cart_wrench_base_vec_outport_.write(m_cart_wrench_base_vec_);
    
	filterDeadZone();
	
    cart_wrench_ee_kdl_outport_.write(m_cart_wrench_ee_kdl_filt_);
    cart_wrench_ee_vec_outport_.write(m_cart_wrench_ee_vec_);

    cart_pos_kdl_outport_.write(T_ee_base_);
    cart_vel_kdl_outport_.write(m_cart_twist_kdl_);
	

    tool_fx_outport_.write(m_cart_wrench_ee_kdl_filt_.force[0] ); //m_cart_wrench_ee_kdl_.force[0] - wrench_offset.force[0]);
    tool_fy_outport_.write(m_cart_wrench_ee_kdl_filt_.force[1] ); //m_cart_wrench_ee_kdl_.force[1] - wrench_offset.force[1]);
    tool_fz_outport_.write(m_cart_wrench_ee_kdl_filt_.force[2] ); //m_cart_wrench_ee_kdl_.force[2] - wrench_offset.force[2]);
    tool_tx_outport_.write(m_cart_wrench_ee_kdl_filt_.torque[0]); //m_cart_wrench_ee_kdl_.torque[0] - wrench_offset.torque[0]);
    tool_ty_outport_.write(m_cart_wrench_ee_kdl_filt_.torque[1]); //m_cart_wrench_ee_kdl_.torque[1] - wrench_offset.torque[1]);
    tool_tz_outport_.write(m_cart_wrench_ee_kdl_filt_.torque[2]); //m_cart_wrench_ee_kdl_.torque[2] - wrench_offset.torque[2]);

    // Write to the joint torque output ports
    joint_torque_1_outport_.write(joint_eff_eigen_[0]);
    joint_torque_2_outport_.write(joint_eff_eigen_[1]);
    joint_torque_3_outport_.write(joint_eff_eigen_[2]);
    joint_torque_4_outport_.write(joint_eff_eigen_[3]);
    joint_torque_5_outport_.write(joint_eff_eigen_[4]);
    joint_torque_6_outport_.write(joint_eff_eigen_[5]);
    joint_torque_7_outport_.write(joint_eff_eigen_[6]);
                                                                      
} // updateHook

void Jacobian_calculator::stopHook() {
    std::cout << "Jacobian_calculator executes stopping !" <<std::endl;
}

void Jacobian_calculator::cleanupHook() {
    std::cout << "Jacobian_calculator cleaning up !" <<std::endl;
}

bool Jacobian_calculator::checkJointNames() {
    // check urdf joint names and compare the names coming in the port
    // This is done to make sure the same indexing for joints
    
    // TODO: implement
    // joint_names_ comes from urdf
    // m_joint_eff_mc_.names contains the names
    for (int i=0; i < NUMBER_OF_JOINTS ; i++)
    {
        if (joint_names_[i] != m_joint_state_js_.name[i] )
            return false;
    }
    return true;
}

void Jacobian_calculator::calibrate() {
    
    if (message_type_ == VECTOR)
        port_status_ = joint_eff_vec_inport_.read(m_joint_eff_vec_);
    else if (message_type_ == ROS)
        port_status_ = joint_state_js_inport_.read(m_joint_state_js_);

    if (port_status_ == RTT::NoData)
        Jacobian_calculator::calibrate();

    if (message_type_ == VECTOR)
        m_torque_offset_vec_ = m_joint_eff_vec_;
    else if (message_type_ == ROS)
        m_torque_offset_vec_ = m_joint_state_js_.effort;

}


int Jacobian_calculator::getNumberOfJoints(){
    RTT::Logger::In in(this->getName());
    RTT::log() << "\n---------------------------" << RTT::endlog();
    RTT::log() << " \t number of joints: " << NUMBER_OF_JOINTS << RTT::endlog();
    RTT::log() << "---------------------------\n" << RTT::endlog();
    return NUMBER_OF_JOINTS;
}


void Jacobian_calculator::setWrenchOffset(){
	// This operation is used to store an offset wrench and then later use it
	// to filter the wrench measurement so that it is closer to zero
	wrench_offset =  m_cart_wrench_ee_kdl_;	
	std::cout << "Successfully reset the wrench offset!" << std::endl;
}

void Jacobian_calculator::filterDeadZone()
{
		// Write each element of the wrench vector to an output port	
// 		deadzone_fx = 3;
// 		deadzone_fy = 3;
// 		deadzone_fz = 3;
// 		deadzone_tx = 0.65;
// 		deadzone_ty = 0.65;
// 		deadzone_tz = 0.65;
		
		// filter force-x
		if ((m_cart_wrench_ee_kdl_.force[0] < deadzone_fx) && (m_cart_wrench_ee_kdl_.force[0] > -deadzone_fx))
		{
			m_cart_wrench_ee_kdl_filt_.force[0]= 0;
		}
		else
		{
			if (m_cart_wrench_ee_kdl_.force[0] >= deadzone_fx)
			{
				m_cart_wrench_ee_kdl_filt_.force[0] = m_cart_wrench_ee_kdl_.force[0] - deadzone_fx;
			}
			else
			{
				m_cart_wrench_ee_kdl_filt_.force[0] = m_cart_wrench_ee_kdl_.force[0] + deadzone_fx;
			}
		}

		// filter force-y
		if ((m_cart_wrench_ee_kdl_.force[1] < deadzone_fy) && (m_cart_wrench_ee_kdl_.force[1] > -deadzone_fy))
		{
			m_cart_wrench_ee_kdl_filt_.force[1]= 0;
		}
		else
		{
			if (m_cart_wrench_ee_kdl_.force[1] >= deadzone_fy)
			{
				m_cart_wrench_ee_kdl_filt_.force[1] = m_cart_wrench_ee_kdl_.force[1] - deadzone_fy;
			}
			else
			{
				m_cart_wrench_ee_kdl_filt_.force[1] = m_cart_wrench_ee_kdl_.force[1] + deadzone_fy;
			}
		}
		
		// filter force-z
		if ((m_cart_wrench_ee_kdl_.force[2] < deadzone_fz) && (m_cart_wrench_ee_kdl_.force[2] > -deadzone_fz))
		{
			m_cart_wrench_ee_kdl_filt_.force[2]= 0;
		}
		else
		{
			if (m_cart_wrench_ee_kdl_.force[2] >= deadzone_fz)
			{
				m_cart_wrench_ee_kdl_filt_.force[2] = m_cart_wrench_ee_kdl_.force[2] - deadzone_fz;
			}
			else
			{
				m_cart_wrench_ee_kdl_filt_.force[2] = m_cart_wrench_ee_kdl_.force[2] + deadzone_fz;
			}
		}

		// filter torque-x
		if ((m_cart_wrench_ee_kdl_.torque[0] < deadzone_tx) && (m_cart_wrench_ee_kdl_.torque[0] > -deadzone_tx))
		{
			m_cart_wrench_ee_kdl_filt_.torque[0]= 0;
		}
		else
		{
			if (m_cart_wrench_ee_kdl_.torque[0] >= deadzone_tx)
			{
				m_cart_wrench_ee_kdl_filt_.torque[0] = m_cart_wrench_ee_kdl_.torque[0]- deadzone_tx;
			}
			else
			{
				m_cart_wrench_ee_kdl_filt_.torque[0] = m_cart_wrench_ee_kdl_.torque[0] + deadzone_tx;
			}
		}
		
		// filter torque-y
		if ((m_cart_wrench_ee_kdl_.torque[1] < deadzone_ty) && (m_cart_wrench_ee_kdl_.torque[1] > -deadzone_ty))
		{
			m_cart_wrench_ee_kdl_filt_.torque[1]= 0;
		}
		else
		{
			if (m_cart_wrench_ee_kdl_.torque[1] >= deadzone_ty)
			{
				m_cart_wrench_ee_kdl_filt_.torque[1] = m_cart_wrench_ee_kdl_.torque[1] - deadzone_ty;
			}
			else
			{
				m_cart_wrench_ee_kdl_filt_.torque[1] = m_cart_wrench_ee_kdl_.torque[1] + deadzone_ty;
			}
		}
		
		// filter torque-z
		if ((m_cart_wrench_ee_kdl_.torque[2] < deadzone_tz) && (m_cart_wrench_ee_kdl_.torque[2] > -deadzone_tz))
		{
			m_cart_wrench_ee_kdl_filt_.torque[2]= 0;
		}
		else
		{
			if (m_cart_wrench_ee_kdl_.torque[2] >= deadzone_tz)
			{
				m_cart_wrench_ee_kdl_filt_.torque[2] = m_cart_wrench_ee_kdl_.torque[2] - deadzone_tz;
			}
			else
			{
				m_cart_wrench_ee_kdl_filt_.torque[2] = m_cart_wrench_ee_kdl_.torque[2] + deadzone_tz;
			}
		}			
}

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Jacobian_calculator)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Jacobian_calculator)
